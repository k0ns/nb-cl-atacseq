import pandas as pd

WORKING_DIR = "/fast/users/helmsauk_c/work/nb-cl-atacseq/"
FASTQ_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-atacseq/fastq/"
metadata = pd.read_csv(WORKING_DIR + "metadata.csv", sep=";")

rule all:
  input:
    expand(FASTQ_DIR + "{e}.{r}.fq.gz", e=metadata.SAMPLE.tolist(), r=["R1", "R2"])

rule get_data:
  output:
      FASTQ_DIR + "{e}.R1.fq.gz",
      FASTQ_DIR + "{e}.R2.fq.gz"
  params:
      e = lambda wildcards: {wildcards.e},
      srr = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].SRR.values[0],
      is_local = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].LOCAL_R1.notnull().values[0],
      is_ftp = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].FTP_R1.notnull().values[0],
      ftp_r1 = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].FTP_R1.values[0],
      ftp_r2 = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].FTP_R2.values[0],
      local_r1 = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].LOCAL_R1.values[0],
      local_r2 = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].LOCAL_R2.values[0],
      fq_dir = FASTQ_DIR
  conda:
      WORKING_DIR + "sra.yaml"
  run:
      if params.is_local:
          shell("cp {params.local_r1} {params.fq_dir}{params.e}.R1.fq.gz && cp {params.local_r2} {params.fq_dir}{params.e}.R2.fq.gz")
      elif params.is_ftp:
          shell("rm -f {params.fq_dir}{params.srr}.R1.fastq.gz && wget {params.ftp_r1} -P {params.fq_dir} && mv {params.fq_dir}{params.srr}_1.fastq.gz {params.fq_dir}{params.e}.R1.fq.gz && rm -f {params.fq_dir}{params.srr}.R2.fastq.gz && wget {params.ftp_r2} -P {params.fq_dir} && mv {params.fq_dir}{params.srr}_2.fastq.gz {params.fq_dir}{params.e}.R2.fq.gz")

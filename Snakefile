import pandas as pd

WORKING_DIR = "/fast/users/helmsauk_c/work/nb-cl-atacseq/"
FASTQ_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-atacseq/fastq/"
BAM_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-atacseq/bam/"
MACS2_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-atacseq/MACS2/"
SVABA_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-atacseq/svaba/"


REFERENCE_FASTA = "/fast/users/helmsauk_c/scratch/hg19_bwa/hg19.fa"
BBMAP_DIR = "/fast/users/helmsauk_c/work/miniconda/envs/qc/opt/bbmap-38.58-0/"
ADAPTER_SEQUENCES = BBMAP_DIR + "resources/adapters.fa"
PICARD_JAR = '/fast/users/helmsauk_c/work/Applications/picard-2.20.4/picard.jar'
TMP_DIR = "/fast/users/helmsauk_c/scratch/tmp"
SAMTOOLS_PATH = "/fast/users/helmsauk_c/work/miniconda/bin/samtools"
SVABA_BIN_PATH = "/fast/users/helmsauk_c/work/svaba/bin"

# mkdir /fast/users/helmsauk_c/work/resources/blacklists/ && cd /fast/users/helmsauk_c/work/resources/blacklists/
# wget http://hgdownload.cse.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeDacMapabilityConsensusExcludable.bed.gz
# gunzip wgEncodeDacMapabilityConsensusExcludable.bed.gz
# bedtools merge -i wgEncodeDacMapabilityConsensusExcludable.bed > wgEncodeDacMapabilityConsensusExcludable.mergeBed.bed
MAPPABILITY_BLACKLIST = "/fast/users/helmsauk_c/work/resources/blacklists/wgEncodeDacMapabilityConsensusExcludable.mergeBed.bed"

metadata = pd.read_csv(WORKING_DIR + "metadata.csv", sep=";")
EXP = metadata.SAMPLE.tolist()

rule all:
    input:
        expand(FASTQ_DIR + "{exp}.{r}_fastqc.html", exp=EXP, r=["R1", "R2"]),
        expand(FASTQ_DIR + "{exp}.{r}.trimmed_fastqc.html", exp=EXP, r=["R1", "R2"]),
        expand(BAM_DIR + "{exp}.trimmed.hg19.rmdup.bw", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.bw", exp=EXP),
        expand(MACS2_DIR + "{exp}/{exp}_MACS2_peaks.narrowPeak", exp=EXP),
        expand(MACS2_DIR + "{exp}/{exp}_MACS2_peaks.narrowPeak.filtered.bed", exp=EXP),
        WORKING_DIR + "multiqc_report.html",
	    expand(BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.allgenes_deeptools.png", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.housekeeping_deeptools.png", exp=EXP)


rule fastqc:
    input:
        "{exp}.fq.gz"
    output:
        "{exp}_fastqc.html"
    conda:
        "envs/qc.yaml"
    shell:
        "fastqc {input}"

rule adapter_trimming:
    input:
        r1 = FASTQ_DIR + "{exp}.R1.fq.gz",
        r2 = FASTQ_DIR + "{exp}.R2.fq.gz"
    output:
        r1_trimmed = FASTQ_DIR + "{exp}.R1.trimmed.fq.gz",
        r2_trimmed = FASTQ_DIR + "{exp}.R2.trimmed.fq.gz"
    conda:
        "envs/qc.yaml"
    shell:
        BBMAP_DIR + "bbduk.sh in1={input.r1} in2={input.r2} out1={output.r1_trimmed} out2={output.r2_trimmed} ktrim=r k=23 mink=11 hdist=1 ref=" + ADAPTER_SEQUENCES + " tbo"

rule bwa_index:
    input:
        REFERENCE_FASTA
    output:
        REFERENCE_FASTA + ".bwt"
    params:
        samtools_bin = SAMTOOLS_PATH
    shell:
        "{params.samtools_bin} faidx {input} && bwa index {input}"

rule alignment:
    input:
        ref = REFERENCE_FASTA,
        ref_bwa_index = REFERENCE_FASTA + ".bwt",
        r1 = FASTQ_DIR +  "{exp}.R1.trimmed.fq.gz",
        r2 = FASTQ_DIR +  "{exp}.R2.trimmed.fq.gz"
    output:
        bam = BAM_DIR + "{exp}.hg19.bam"
    conda:
        "envs/bwa.yaml"
    params:
        threads = "10",
        samtools_bin = SAMTOOLS_PATH
    shell:
        "bwa mem {input.ref} {input.r1} {input.r2} -t {params.threads} | {params.samtools_bin} sort -@{params.threads} -o {output.bam} - && {params.samtools_bin} index {output.bam} && {params.samtools_bin} stats {output.bam} > {output.bam}.stats.txt"

# Thought about including a | samtools view -h -f 3 -F 4 -F 8 -F 256 -F 2048 in the pipeline above. Should I do that?
# -f 3: only include alignments marked with the SAM flag 3, which means "properly paired and mapped"
# -F 4: exclude aligned reads with flag 4: the read itself did not map
# -F 8: exclude aligned reads with flag 8: their mates did not map
# -F 256: exclude alignments with flag 256, which means that bwa mapped the read to multiple places in the reference genome, and this alignment is not the best
# -F 1024: exclude alignments marked with SAM flag 1024, which indicates that the read is an optical or PCR duplicate (this flag would be set by Picard)
# -F 2048: filter out chimeric reads
# -q 30: minimum mapping quality 30

rule remove_duplicates:
    input:
        bam = BAM_DIR + "{exp}.hg19.bam"
    output:
        bam_markdup = BAM_DIR + "{exp}.hg19.rmdup.bam",
        bam_markdup_metrics = BAM_DIR + "{exp}.hg19.rmdup-metrics.txt"
    params:
        tmp_dir = TMP_DIR,
        picard_jar = PICARD_JAR
    conda:
        "envs/arima-mapping.yaml"
    shell:
        "java -Xmx30G -XX:-UseGCOverheadLimit -Djava.io.tmpdir={params.tmp_dir} -jar {params.picard_jar} MarkDuplicates INPUT={input.bam} OUTPUT={output.bam_markdup} METRICS_FILE={output.bam_markdup_metrics} TMP_DIR={params.tmp_dir} REMOVE_DUPLICATES=TRUE MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 CREATE_INDEX=TRUE"

rule raw_bigWig:
    input:
        BAM_DIR + "{exp}.hg19.rmdup.bam"
    output:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.bw"
    conda:
        "envs/deeptools.yaml"
    shell:
        "bamCoverage --bam {input} -o {output}"

rule filtered_bigWig:
    input:
        BAM_DIR + "{exp}.hg19.rmdup.bam"
    output:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.bw"
    params:
        blacklist = MAPPABILITY_BLACKLIST
    conda:
        "envs/deeptools.yaml"
    shell:
        "bamCoverage --bam {input} -o {output} --blackListFileName {params.blacklist} --normalizeUsing CPM -ignore chrM"

rule macs2:
    input:
        BAM_DIR + "{exp}.hg19.rmdup.bam"
    output:
        MACS2_DIR + "{exp}/{exp}_MACS2_peaks.narrowPeak"
    params:
        outdir = MACS2_DIR + "{exp}/",
        sample = "{exp}_MACS2"
    conda:
        "envs/macs2.yaml"
    shell:
        "macs2 callpeak --treatment {input} --name {params.sample} --outdir {params.outdir} -g 2.7e9 --shift -75 --extsize 150 --nomodel -B --SPMR --call-summits"

rule peak_exclude_blacklist:
    input:
        "{peakfile}Peak"
    output:
        "{peakfile}Peak.filtered.bed"
    params:
        blacklist = MAPPABILITY_BLACKLIST
    conda:
        "envs/bedtools.yaml"
    shell:
        "bedtools intersect -a {input} -b {params.blacklist} -v > {output}"

rule multiqc:
    input:
        expand(FASTQ_DIR + "{exp}.{r}_fastqc.html", exp=EXP, r=["R1", "R2"]),
        expand(FASTQ_DIR + "{exp}.{r}.trimmed_fastqc.html", exp=EXP, r=["R1", "R2"]),
        expand(BAM_DIR + "{exp}.trimmed.hg19.rmdup.bw", exp=EXP),
        expand(MACS2_DIR + "{exp}/{exp}_MACS2_peaks.narrowPeak", exp=EXP),
        expand(MACS2_DIR + "{exp}/{exp}_MACS2_peaks.narrowPeak.filtered.bed", exp=EXP)
    output:
        WORKING_DIR + "multiqc_report.html"
    conda:
        "envs/qc.yaml"
    shell:
        "multiqc -f " + FASTQ_DIR + " " + BAM_DIR + " " + MACS2_DIR

rule svaba:
    input:
        BAM_DIR + "{exp}.hg19.rmdup.bam"
    output:
        sv = SVABA_DIR + "{exp}.svaba.sv.vcf.gz",
        indel = SVABA_DIR + "{exp}.svaba.indel.vcf.gz"
    params:
        samtools_path = SAMTOOLS_PATH,
        threads = 10,
        reference_genome_for_bwa = REFERENCE_FASTA,
        exp = SVABA_DIR + "{exp}",
        blacklist_bed = "/fast/users/helmsauk_c/work/resources/svaba-support/svaba_exclusions.bed" # from https://data.broadinstitute.org/snowman/
    shell:
        SAMTOOLS_PATH + "index {input.bam} && " + SVABA_BIN_PATH + "svaba run -z -t {input.bam} -a {params.exp} -p {params.threads} -G {params.reference_genome_for_bwa} --germline --read-tracking --blacklist {params.blacklist_bed}"

rule heatmap_make_matrix_all_genes:
    input:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.bw"
    output:
        temp(BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.allgenes_deeptools.mat.gz")
    params:
        genes = "/fast/users/helmsauk_c/work/resources/rseqc-support/hg19_Ensembl_gene.bed",
        threads = "8"
    conda:
        "envs/deeptools.yaml"
    shell:
        "computeMatrix scale-regions -S {input} -R {params.genes} --beforeRegionStartLength 3000 --regionBodyLength 5000 --afterRegionStartLength 3000 --skipZeros -o {output} -p {params.threads}"

rule heatmap_plot_all_genes:
    input:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.allgenes_deeptools.mat.gz"
    output:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.allgenes_deeptools.png"
    params:
        sample = "{exp}"
    conda:
        "envs/deeptools.yaml"
    shell:
        "plotHeatmap -m {input} -out {output} --colorMap RdBu --samplesLabel {params.sample}"

rule heatmap_make_matrix_housekeeping:
    input:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.bw"
    output:
        temp(BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.housekeeping_deeptools.mat.gz")
    params:
        housekeeping_genes = "/fast/users/helmsauk_c/work/resources/rseqc-support/hg19.HouseKeepingGenes.bed",
        threads = "8"
    conda:
        "envs/deeptools.yaml"
    shell:
        "computeMatrix scale-regions -S {input} -R {params.housekeeping_genes} --beforeRegionStartLength 3000 --regionBodyLength 5000 --afterRegionStartLength 3000 --skipZeros -o {output} -p {params.threads}"

rule heatmap_plot_housekeeping:
    input:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.housekeeping_deeptools.mat.gz"
    output:
        BAM_DIR + "{exp}.trimmed.hg19.rmdup.filterednormed.housekeeping_deeptools.png"
    params:
        sample = "{exp}"
    conda:
        "envs/deeptools.yaml"
    shell:
        "plotHeatmap -m {input} -out {output} --colorMap RdBu --samplesLabel {params.sample}"

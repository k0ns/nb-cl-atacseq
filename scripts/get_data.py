import pandas as pd
import os
import sys

metadata = pd.read_csv(snakemake.input.metadata, sep=";")

sample = snakemake.wildcards.sample
srr = metadata[metadata.SAMPLE == snakemake.wildcards.sample].SRR.values[0]
is_not_local = metadata[metadata.SAMPLE == snakemake.wildcards.sample].LOCAL_PATH.isnull().values[0]
is_not_ftp = metadata[metadata.SAMPLE == snakemake.wildcards.sample].FASTQ_FTP_PREFIX.isnull().values[0]
ftp_prefix = metadata[metadata.SAMPLE == snakemake.wildcards.sample].FASTQ_FTP_PREFIX.values[0]
local_path = metadata[metadata.SAMPLE == snakemake.wildcards.sample].LOCAL_PATH.values[0]
fq_dir = snakemake.params.fq_dir

if is_not_ftp:
    os.system("cp " + {params.local_path}.fastq.gz {params.fq_dir}{params.e}.fq.gz")
elif is_not_local:
    os.system("rm -f {params.fq_dir}{params.srr}.fastq.gz && wget {params.ftp_prefix}.fastq.gz -P {params.fq_dir} && mv {params.fq_dir}{params.srr}.fastq.gz {params.fq_dir}{params.e}.fq.gz")
else:
    error("The sample is neither local nor has a ftp link.")
